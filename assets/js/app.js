/**
 * app.js
 *
 * This file contains some conventional defaults for working with Socket.io + Sails.
 * It is designed to get you up and running fast, but is by no means anything special.
 *
 * Feel free to change none, some, or ALL of this file to fit your needs!
 */


(function (io) {

  // as soon as this file is loaded, connect automatically, 
  var socket = io.connect();
  if (typeof console !== 'undefined') {
    log('Connecting to Sails.js...');
  }

  io.socket.on('connect', function socketConnected() 
  {

    console.log("This is from the connect: ", this.socket.sessionid);

    // Listen for the socket 'message'
    console.log(socket);
    
    // Subscribe to the user model classroom and instance room
    socket.on('users', cometMessageReceivedFromServer);
    // socket.on('users', function notificationReceivedFromServer ( message ) {
      
    //   console.log("New comet message received :: ", message);
    //   log('New comet message received :: ', message);

    // });

    socket.get('/users/subscribe', function gotResponse (res) 
    {

      console.log("In socket get function ...");
      console.log(res);
      console.log(socket.on);
      console.log("res in socket get function");
    });

    // io.socket.get('/users/subscribe', {}, function(res) 
    // {
    //   console.log("In socket get function ...");
    //   console.log(res); // or whatever you want to do with the existing models the first time you get them on pageload
    //   console.log("res in socket get function");
    // });

    

    // io.socket.on('message', function(message) 
    // {
    //   console.log("in Socket On Function message start...");
    //   console.log(message);
    //   console.log("in Socket On Function message stop...");
    // });

    // io.socket.on('user', function(data) 
    // {
    //   console.log("in Socket On Function User start...");
    //   console.log(data);
    //   console.log("in Socket On Function User stop...");
    // });
    ///////////////////////////////////////////////////////////
    // Here's where you'll want to add any custom logic for
    // when the browser establishes its socket connection to 
    // the Sails.js server.
    ///////////////////////////////////////////////////////////
    log(
        'Socket is now connected and globally accessible as `socket`.\n' + 
        'e.g. to send a GET request to Sails, try \n' + 
        '`socket.get("/", function (response) ' +
        '{ console.log(response); })`'
    );
    ///////////////////////////////////////////////////////////

  });


  // Expose connected `socket` instance globally so that it's easy
  // to experiment with from the browser console while prototyping.
  window.socket = io.socket;


  // Simple log function to keep the example simple
  function log () {
    if (typeof console !== 'undefined') {
      console.log.apply(console, arguments);
    }
  }
  

})(

  // In case you're wrapping socket.io to prevent pollution of the global namespace,
  // you can replace `window.io` with your own `io` here:
  window.io

);

// This function routes the message based upon the model which issued it
function cometMessageReceivedFromServer(message) 
{

  console.log("Here's the message: ", message);

  // Okay, I need to route this message to the appropriate place.

  // This message has to do with the User Model
  
  if(message.verb === 'destroyed') 
  {
    // What page am I on?
    var page = document.location.pathname;

    // Strip trailing slash if we've got one
    page = page.replace(/(\/)$/, '');
    
    console.log(page);
    console.log("Message Verb:" + message.verb);
    // Route to the appropriate user update handler based on which page you're on
    switch (page) 
    {
      
      // If we're on the User Administration Page (a.k.a. user index)
      case '/users':
        console.log("in users Statment");
        // This is a message coming from publishUpdate
        if (message.verb === 'destroyed') 
        {
          $('tr[data-id="' + message.id + '"]').remove();
        }
        break;
    }
  }
  else
  {
    console.log("Checking for USer Model Found");
    if (message.data.model === 'users') 
    {
      console.log("Model Found");
      var userId = message.id
      updateUserInDom(userId, message);
      
      if(message.verb !== "destroyed") 
      {
        displayFlashActivity(message);  
      } 
    }
  };
}


function displayFlashActivity(message) 
{
  $('#chatAudio')[0].play();
  $("#flash_alert").after("<div class='alert alert-success'>" + message.data.name + message.data.action + "</div>");
  $(".alert").fadeOut(5000);
}

function updateUserInDom(userId, message) {

  // What page am I on?
  var page = document.location.pathname;

  // Strip trailing slash if we've got one
  page = page.replace(/(\/)$/, '');
  
  console.log(page);
  console.log("Message Verb:" + message.verb);
  console.log("message loggedIn::" + message.data.loggedIn);
  // Route to the appropriate user update handler based on which page you're on
  switch (page) {
    
    // If we're on the User Administration Page (a.k.a. user index)
    case '/users':
      console.log("in users Statment");
      // This is a message coming from publishUpdate
      if (message.verb === 'updated') 
      {
        if (message.data.loggedIn) 
        {
          var $userRow = $('tr[data-id="' + message.data.id + '"] td img').first();
          $userRow.attr('src', "/images/icon-online.png");
        } 
        else 
        {
          var $userRow = $('tr[data-id="' + message.data.id + '"] td img').first();
          $userRow.attr('src', "/images/icon-offline.png");
        };
      }
      
      break;
  }
}


/////////////////////////////////////////////////
// User index page DOM manipulation logic
// (i.e. backbone-style view)
/////////////////////////////////////////////////
var UserIndexPage = {

  // Update the User, in this case their login status
  updateUser: function(id, message) {
    if (message.data.loggedIn) {
      var $userRow = $('tr[data-id="' + id + '"] td img').first();
      $userRow.attr('src', "/images/icon-online.png");
    } else {
      var $userRow = $('tr[data-id="' + id + '"] td img').first();
      $userRow.attr('src', "/images/icon-offline.png");
    }
  }

}