/**
 * SessionController
 *
 * @description :: Server-side logic for managing sessions
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

var bcrypt = require('bcrypt');

module.exports = {
	
	'new': function(req, res) {
		//res.locals.flash= _.clone(req.session.flash);
		res.view();
		//res.locals.flash = {};		
	},

	create: function(req, res, next) {

		// Check for email and password in params sent via the form, if none
		// redirect the browser back to the sign-in form.
		console.log(req.body);
		if (!req.param('email') || !req.param('password')) {
			// return next({err: ["Password doesn't match password confirmation."]});

			var usernamePasswordRequiredError = [{
				name: 'usernamePasswordRequired',
				message: 'You must enter both a username and password.'
			}]

			// Remember that err is the object being passed down (a.k.a. flash.err), whose value is another object with
			// the key of usernamePasswordRequiredError
			req.session.flash = {
				err: usernamePasswordRequiredError
			}

			res.redirect('/session/new');
			return;
		}

		// Try to find the user by there email address. 
		// findOneByEmail() is a dynamic finder in that it searches the model by a particular attribute.
		// User.findOneByEmail(req.param('email')).done(function(err, user) {
		
		Users.findOneByEmail(req.body.email).exec(function foundUser(err, user) 
		{
			if (err) return next(err);

			// If no user is found...
			if (!user) 
			{
				var noAccountError = [{
					name: 'noAccount',
					message: 'The email address ' + req.param('email') + ' not found.'
				}]
				req.session.flash = {
					err: noAccountError
				}
				res.redirect('/session/new');
				return;
			}

			// Compare password from the form params to the encrypted password of the user found.
			bcrypt.compare(req.param('password'), user.encryptedPassword, function(err, valid) {
				if (err) return next(err);

				// If the password from the form doesn't match the password from the database...
				if (!valid) {
					var usernamePasswordMismatchError = [{
						name: 'usernamePasswordMismatch',
						message: 'Invalid username and password combination.'
					}]
					req.session.flash = {
						err: usernamePasswordMismatchError
					}
					res.redirect('/session/new');
					return;
				}

				// Log user in
				req.session.authenticated = true;

				req.session.User = user;
				user.online = true;
				user.save(function(err, user) 
				{
				 	if (err) return next(err);

				 	// Inform other sockets (e.g. connected sockets that are subscribed) that this user is now logged in
					console.log("publishUpdate start....");
					
					Users.publishUpdate(user.id, {
				        loggedIn: true,
				        id: user.id,
				        name: user.name,
				        model: 'users',
				        action: ' has logged in.'
				    });

					console.log("publishUpdate stop....");
					
					if (req.session.User.admin)	
					{
						res.redirect('/users');
						return;
					};
					res.redirect('/users/show/' + user.id);

				});
			});
		});
	},

	destroy: function(req, res, next) {

		
		
		Users.findOne(req.session.User.id).exec(function foundUser(err, user) {
			var userid = req.session.User.id;
			Users.update(userid,{
				online: false
			}, 
			function(err)
			{
				if (err) return next(err);
				console.log("publishUpdate in destroy start....");
				Users.publishUpdate(userid, {
				        loggedIn: false,
				        id: user.id,
				        name: user.name,
				        model: 'users',
				        action: ' has logged Out.'
				    });
				console.log("publishUpdate in destroy stop....");
				req.session.destroy();

				// Redirect the browser to the sign-in screen
	
				res.redirect('/');

			});
		});
	}

};

