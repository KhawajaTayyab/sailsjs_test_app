/**
 * UsersController
 *
 * @description :: Server-side logic for managing users
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {
	'new': function (req, res) 
	{
		//res.locals.flash= _.clone(req.session.flash);
		res.view();
		//res.locals.flash = {};		
	},

	create: function (req, res, next)
	{
		console.log(req.allParams());
		Users.create({name: req.body.name, title: req.body.title, email: req.body.email, encryptedPassword: req.body.password, confirmation: req.body.confirmation}).exec(function userCreated(err, user)
		{
			if(err)
			{
				console.log(err);
				req.session.flash = {
					err:err
				}
				return res.redirect('/users/new');
			}
			else
			{
				//res.json(user);
				req.session.authenticated = true;
				req.session.User = user;
				user.online = true;
				user.save(function(err, user)
				{
					if (err) 
					{
						req.session.flash = {
							err:err
						}
						return res.redirect('/users/new');
					}
					else
					{
						return res.redirect('/users/show/'+user.id);
					};

				});
			};
		})
	},

	show: function (req, res, next){
		console.log(req.session.authenticated);
		Users.findOne(req.params['id']).exec(function foundUser(err, user)
		{
			if (err)
			{
				return res.redirect('/');
			}
			else
			{
				res.view({
					user: user
				});
			};
		});
	},

	index: function (req,res, next){
		Users.find().exec(function allUser(err, users)
		{
			if (err)
			{
				return res.redirect('/');
			}
			else
			{
				res.view({
					users: users
				});
			};
		});
	},

	edit: function (req, res, next){
		Users.findOne(req.params['id']).exec(function foundUser(err, user)
		{
			if (err)
			{
				return res.redirect('/users/');
			}
			else
			{
				res.view({
					user: user
				});
			};
		});
	},

	update: function (req, res, next){
		if (req.session.User.admin) 
		{
			console.log("User Admin");
			console.log(req.params.admin);
	      	var userObj = {
		        name: req.body.name,
		        title: req.body.title,
		        email: req.body.email,
		        admin: req.body.admin
	      	}	
	    }
	    else 
	    {
	    	console.log("User Not Admin");
		    var userObj = {
		        name: req.body.name('name'),
		        title: req.body.title,
		        email: req.body.email
	      	}
	    }

		Users.update(req.params['id'], userObj).exec(function foundUser(err, user)
		{
			if (err)
			{
				console.log(err);
				return res.redirect('/users/edit/' + req.params['id']);
			}
			else
			{
				res.redirect('/users/show/' + req.params['id']);
			};
		});
	},

	destroy: function (req,res, next)
	{
		Users.findOne(req.params['id']).exec(function foundUser(err, user)
		{
			if (err)
			{
				console.log(err);
				res.redirect("/users/");
			}
			else
			{
				Users.destroy(req.params['id']).exec(function userDestroyed(err)
				{
					if (err)
					{
						console.log(err);
						res.redirect('/users/');
					}
					else
					{
					    // Inform other sockets (e.g. connected sockets that are subscribed) that this user is now logged in
				        Users.publishUpdate(user.id, 
				        {
				          model: 'users',
				          name: user.name,
				          action: ' has been destroyed.'
				        });

				        // Let other sockets know that the user instance was destroyed.
        				Users.publishDestroy(user.id);

						res.redirect('/users/');
					}
				});
			};

		});
	},


	// This action works with app.js socket.get('/users/subscribe') to
  	// subscribe to the User model classroom and instances of the user
  	// model
  	subscribe: function(req, res)
  	{
 		console.log("in subscribe");
	    // Find all current users in the user model
	    Users.find().exec(function foundUsers(err, users) 
	    {
	    	console.log("in subscribe all user");
	      	if (err) return next(err);
	 
			// subscribe this socket to the User model classroom
			//Users.subscribe(req.socket, ['message']);

			// subscribe this socket to the user instance rooms
			console.log("subscribe Started......");
			console.log("Socket" + req.socket);
			console.log("Users" + users);
			Users.subscribe(req.socket, users);
			console.log("subscribe Stoped......");

			// This will avoid a warning from the socket for trying to render
			// html over the socket.
			res.send(200);
	    });
  	}
};

