/**
 * sessionAuth
 *
 * @module      :: Policy
 * @description :: Simple policy to allow any authenticated user
 *                 Assumes that your login action in one of your controllers sets `req.session.authenticated = true;`
 * @docs        :: http://sailsjs.org/#!documentation/policies
 *
 */
module.exports = function(req, res, ok) {

  // User is allowed, proceed to the next policy, 
  // or if this is the last policy, the controller
  console.log("In Session Auth Policy");
  console.log("Authenticated:"+req.session.authenticated);
    
  if (req.session.authenticated) 
  {
    return ok();
  }

  // User is not allowed
  else 
  {
    console.log(req.session.authenticated);
    var requireLoginError = [{name: 'requireLogin', message: 'You must be signed in.'}]
    req.session.flash = {
    	err: requireLoginError
    }
    res.redirect('/');
    return;
    //res.send(403);
  }
};
