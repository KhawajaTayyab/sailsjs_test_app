
/**
 * Allow a logged-in user to see, edit and update her own profile
 * Allow admins to see everyone
 */

module.exports = function(req, res, ok) {

	console.log("In User Can See Profile Policies");

	var sessionUserMatchesId = req.session.User.id == req.param('id');
	console.log("User Session ID:"+ req.session.User.id )
	console.log("USerParams Id:"+ req.param('id'))
	var isAdmin = req.session.User.admin;

	// The requested id does not match the user's id,
	// and this is not an admin
	if (!(sessionUserMatchesId || isAdmin)) 
	{
		var noRightsError = [{name: 'noRights', message: 'You must be an admin.'}]
		req.session.flash = {
			err: noRightsError
		}
    	res.redirect('/');
    	return;
	}
	else
	{
		ok();
	};

};