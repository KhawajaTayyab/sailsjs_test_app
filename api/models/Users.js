/**
* Users.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/
var bcrypt = require('bcrypt');

module.exports = {

  schema: true,
  
  attributes: {
  	name: {
  		type: 'string',
  		required: true
  	},

  	title: {
  		type: 'string'
  	},

  	email: {
  		type: 'string',
  		required: true,
  		email: true,
  		unique: true
  	},

  	encryptedPassword: {
  		type: 'string'
  	},

    confirmation: {
      type: 'string'
    },

    admin: {
      type: 'boolean',
      defaultsTo: false
    },

    online: {
      type: 'boolean',
      defaultsTo: false
    },

    // Lifecycle Callbacks

  },

  beforeValidation: function (values, next) 
  {
    
    if (typeof values.admin !== 'undefined') 
    {
      if (values.admin === 'unchecked') 
      {
        values.admin = false;
      }
      else if (values.admin === false) 
      {
        values.admin = true;
      }
    }
    next();
  },

  beforeCreate: function (values, next) 
  {
    // This checks to make sure the password and password confirmation match before creating record
    if (!values.encryptedPassword || values.encryptedPassword != values.confirmation) {
      return next({err: ["Password doesn't match password confirmation."]});
    }

    require('bcrypt').hash(values.encryptedPassword, 10, function passwordEncrypted(err, data) {
      if (err) return next(err);
      values.encryptedPassword = data;
      values.confirmation = data;
      // values.online= true;
      next();
    });
    
  }
};

